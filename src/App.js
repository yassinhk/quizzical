
import React, { useState } from 'react'
import './app.css'
import Quiz from './Components/Quiz/Quiz';
import Start from './Components/Start/Start';

export default function App() {
  const [quizStart, setQuizStart] = useState(false)

  function startQuiz(){
     setQuizStart(true)
  }

  return (
      <div className="App">
          {
            quizStart ? 
              <Start startQuiz={startQuiz}/>
              :
              <Quiz/>
          }
      </div>    
  )
}

