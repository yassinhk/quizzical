import React from 'react'
import '../../app.css'

export default function Start(props){

    return (
        <div className='start-component'>
            <h1>Quizzical</h1>
            <h4>Answer tricky questions and test your worldly knowledge</h4>
            <button onClick={props.startQuiz}>
                <span>Start quiz</span>
            </button>
        </div>
    )
}