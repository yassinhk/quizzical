import React from 'react'
import './quiz.css'

export default function AnswerItemQuiz(props){
    const style={
        backgroundColor: props.data.isSelected ? "#D6DBF5" : "transparent",
        borderStyle : props.data.isSelected ? "none" : "solid",
        borderWidth : props.data.isSelected ? "0px" : "1px",
        borderColor : props.data.isSelected ? "transparent" : "#293264"
    }

    return (
        <button 
            className='btn-answer'
            style={style}
            onClick={props.selectAnswers}
        >
           <span>{props.data.answer}</span>
        </button>
    )
}