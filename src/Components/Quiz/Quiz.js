import React, { useEffect, useState } from 'react'
import ItemQuiz from './ItemQuiz'
import './quiz.css'
export default function Quiz(){
    
    const [quiz, setQuiz] = useState([])

    useEffect(()=>{
        fetch("https://opentdb.com/api.php?amount=5 ")
            .then(res=>res.json())
            .then(response=>setQuiz(response.results))
    },[])


    const quizElements = quiz.map(
        (element,index) => 
            <ItemQuiz 
                key={index+1} 
                data={element}
            />
    )
    return (
        <main>
             {quizElements}
            <button className='check-answer-btn'>
                <span>Check answers</span>
            </button>
        </main>
    )
}