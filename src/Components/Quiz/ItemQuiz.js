import React, { useEffect, useState } from 'react'
import AnswerItemQuiz from './AnswerItemQuiz'
import './quiz.css'

export default function ItemQuiz(props){

    const [answers, setAnswers] = useState([])
    

    useEffect(()=>{
        //add all incorrect answers and the correct at the end
        //shuffle the array => change the correct answers position
        const tmpAnswers =props.data.incorrect_answers
        tmpAnswers.push(props.data.correct_answer)
        tmpAnswers.sort(()=>Math.random()-0.5)
        //add the answers into object contain {id ,answer, isCorrect, isSelected}
        const _answers = []
        for(let i=0;i<tmpAnswers.length;i++){
            _answers.push({
                id : i+1,
                answer : tmpAnswers[i],
                isSelected :false, 
                isCorrect : tmpAnswers[i]===props.data.correct_answer ?true : false
            })
        }
        //put the array of object into state
        setAnswers(_answers)
    },[ props.data.correct_answer, props.data.incorrect_answers])

    function selectAnswers(id){
        //set Selected answer to true => isSelected : true
        setAnswers(
            prevAnswers=> prevAnswers.map(
                answer => 
                    id=== answer.id ? {...answer,isSelected : true} : {...answer,isSelected:false}
            )
        )
    }

    const answersElements = answers.map(element=>
        <AnswerItemQuiz 
            key={element.id}
            data={element}
            selectAnswers={()=>selectAnswers(element.id)}
        />
    )

    return (
        <div className='item-quiz'>
            <div className='question'>
               {props.data.question}
            </div>
            <div className='answers'>
                {answersElements}
            </div>
            <hr/>
        </div>
    )
}